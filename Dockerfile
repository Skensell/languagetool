FROM maven
COPY . /Skensell/languagetool
WORKDIR /Skensell/languagetool
RUN mvn clean test
RUN ./build.sh languagetool-standalone package -DskipTests

